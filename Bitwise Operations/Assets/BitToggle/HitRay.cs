﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitRay : MonoBehaviour
{
  public Vector3 laserForward;
  public int layerMask;
  public bool sphere;
  public bool cube;
  public bool capsule;

  // Start is called before the first frame update
  void Start()
  {
    laserForward = this.transform.up;
  }

  // Update is called once per frame
  void Update()
  {
    if (sphere) layerMask = (1 << 9) | layerMask;
    else layerMask = layerMask & ~(1 << 9);
    if (cube) layerMask = (1 << 10) | layerMask;
    else layerMask = layerMask & ~(1 << 10);
    if (capsule) layerMask = (1 << 11) | layerMask;
    else layerMask = layerMask & ~(1 << 11);

    RaycastHit hit;
    if (Physics.Raycast(this.transform.position, laserForward, out hit, 20f, layerMask))
    {
      Debug.DrawRay(this.transform.position, laserForward * hit.distance, Color.green);
    }
    else
    {
      Debug.DrawRay(this.transform.position, laserForward * 20f, Color.red);
    }
  }
}
