﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitwiseOperations : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }


  //bool haveSameSign = 0 <= (x ^ y);

  //Swap without a temp
  // x ^= y;
  // y ^= x;
  // x ^= y;

  //Finding the Min and Max of two Values
  // int max = x ^ ((x ^ y) & -((x < y)?1:0));
  // int min = y ^ ((x ^ y) & -((x < y)?1:0));

  /*
  -So often is the case when you are looping over an array there is the need to check a loop counter isn’t out of bounds.  That requires the use of expensive if statements or you can use a bitwise operation.  For example you can do this:
    for( int x = 0, u = 0; x < screen.width; x++, u++ )
    {
      for( int y = 0, v = 0; y < screen.height; y++, v++ )
      {
        int colour = image.getPixel( u, v );
        screen.setPixel( x, y, colour );
  
        // Make sure we don't go outside the image boundary.
        if( u >= image.width )
          u = 0;
        if( v >= image.height )
          v = 0;
      }
    }
  -or you can do this:
    // Assumes image dimensions are powers of 2.
    int widthMask = image.width - 1;
    int heightMask = image.height - 1;
  
    for( int x = 0; x < screen.width; x++ )
    {
      for( int y = 0; x < screen.height; y++ )
      {   
        int colour = image.getPixel( x & widthMask, y & heightMask );
        screen.setPixel( x, y, colour );
      }
    } 
   */

  long Add(long a, long b)
  {
    while (b != 0)
    {
      long c = a & b;
      a = a ^ b;
      b = c << 1;
    }
    return a;
  }

  long subtraction(long a, long b)
  {
    while (b != 0)
    {
      long borrow = (~a) & b;
      a = a ^ b;
      b = borrow << 1;
    }
    return a;
  }

  long Multiply(long n, long m)
  {
    long answer = 0;
    long count = 0;
    while (m != 0)
    {
      if (m % 2 == 1)
        answer += n << (int)count;
      count++;
      m /= 2;
    }
    return answer;
  }

  long remainder = 0;
  long division(long dividend, long divisor)
  {
    long quotient = 1;
    long neg = 1;

    if ((dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0))
      neg = -1;

    // Convert to positive 
    long tempdividend = (long)Mathf.Abs((dividend < 0) ? -dividend : dividend);
    long tempdivisor = (long)Mathf.Abs((divisor < 0) ? -divisor : divisor);

    if (tempdivisor == tempdividend)
    {
      remainder = 0;
      return 1 * neg;
    }
    else if (tempdividend < tempdivisor)
    {
      if (dividend < 0)
        remainder = tempdividend * neg;
      else
        remainder = tempdividend;
      return 0;
    }

    while (tempdivisor << 1 <= tempdividend)
    {
      tempdivisor = tempdivisor << 1;
      quotient = quotient << 1;
    }

    // Call division recursively 
    if (dividend < 0)
      quotient = quotient * neg + division(-(tempdividend - tempdivisor), divisor);
    else
      quotient = quotient * neg + division(tempdividend - tempdivisor, divisor);
    return quotient;
  }
}
