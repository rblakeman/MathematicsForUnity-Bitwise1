﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
  public bool MAGIC;
  public bool INTELLIGENCE;
  public bool CHARISMA;
  public bool FLY;
  public bool INVISIBLE;
  public int doorType = 0;

  // Start is called before the first frame update
  void Start()
  {
    if (MAGIC)
    {
      doorType |= AttributeManager.MAGIC;
    }
    if (INTELLIGENCE)
    {
      doorType |= AttributeManager.INTELLIGENCE;
    }
    if (CHARISMA)
    {
      doorType |= AttributeManager.CHARISMA;
    }
    if (FLY)
    {
      doorType |= AttributeManager.FLY;
    }
    if (INVISIBLE)
    {
      doorType |= AttributeManager.INVISIBLE;
    }
  }

  // Update is called once per frame
  void Update()
  {

  }

  private void OnCollisionEnter(Collision other)
  {
    if ((other.gameObject.GetComponent<AttributeManager>().attributes & doorType) == doorType)
    {
      this.GetComponent<BoxCollider>().isTrigger = true;
    }
  }
  private void OnTriggerExit(Collider other)
  {
    this.GetComponent<BoxCollider>().isTrigger = false;
  }
}
