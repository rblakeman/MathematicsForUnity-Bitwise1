﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Matrix
{
    float[] values;
    int rows;
    int cols;

    public Matrix(int r, int c, float[] v) {
        rows = r;
        cols = c;
        values = new float[rows * cols];
        Array.Copy(v, values, rows * cols);
    }

    // 0=vector, 1=position
    public Matrix(Coords vecpos, float type) {
        rows = 4;
        cols = 1;
        values = new float[4]{ vecpos.x, vecpos.y, vecpos.z, type };
    }

    public Matrix(Coords position) {
        rows = 4;
        cols = 1;
        values = new float[4]{ position.x, position.y, position.z, 1 };
    }

    public Matrix(Matrix a) {
        rows = a.rows;
        cols = a.cols;
        values = new float[rows * cols];
        Array.Copy(a.values, values, rows * cols);
    }

    public Coords AsCoords() {
        if (rows == 4 && cols == 1) {
            return (new Coords(values[0], values[1], values[2], values[3]));
        }

        return null;
    }

    public override string ToString() {
        string matrix = "";

        for (int r = 0; r < rows; r++) {
            matrix += "| ";
            for (int c = 0; c < cols; c++) {
                matrix += values[c + (r * cols)] + " ";
            }
            matrix += "|\n";
        }

        return matrix;
    }

    static public Matrix operator+ (Matrix a, Matrix b) {
        if (a.rows != b.rows || a.cols != b.cols) {
            return null;
        }

        Matrix result = new Matrix(a);

        for (int i = 0; i < a.rows * a.cols; i++) {
            result.values[i] += b.values[i];
        }

        return result;
    }

    static public Matrix operator- (Matrix a, Matrix b) {
        if (a.rows != b.rows || a.cols != b.cols) {
            return null;
        }

        Matrix result = new Matrix(a);

        for (int i = 0; i < a.rows * a.cols; i++) {
            result.values[i] -= b.values[i];
        }

        return result;
    }

    static public Matrix operator* (Matrix a, Matrix b) {
        if (a.cols != b.rows) {
            return null;
        }

        int rows = a.rows;
        int columns = b.cols;
        int length = rows * columns;
        float[] result = new float[length];
        Matrix bTransposed = b.getTranspose();
        // Debug.Log(bTransposed.ToString());

        int idx = 0;
        for (int r = 0; r < rows; r++) {
            float[] aRowValues = new float[1 * a.cols];
            for (int aColumn = 0; aColumn < a.cols; aColumn++) {
                aRowValues[aColumn] = a.values[(r * a.cols) + aColumn];
            }

            for (int c = 0; c < columns; c++) {
                float[] bColumnValues = new float[1 * bTransposed.cols];
                for (int bColumn = 0; bColumn < bTransposed.cols; bColumn++) {
                    bColumnValues[bColumn] = bTransposed.values[(c * bTransposed.cols) + bColumn];
                }

                float temp = 0;
                for (int j = 0; j < a.cols; j++) {
                    temp += aRowValues[j] * bColumnValues[j];
                }
                result[idx] = temp;
                idx++;
            }
        }

        return new Matrix(rows, columns, result);
    }

    Matrix getTranspose () {
        int length = rows * cols;
        float[] result = new float[length];

        int idx = 0;
        for (int c = 0; c < cols; c++) {
            for (int r = 0; r < rows; r++) {
                result[idx] = values[c + (r * cols)];
                idx++;
            }
        }

        return new Matrix(cols, rows, result);
    }
}
