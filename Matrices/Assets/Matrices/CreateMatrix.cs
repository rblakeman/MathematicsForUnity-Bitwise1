﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMatrix : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float[] vals = { 1, 2, 3, 4, 5, 6 };
        Matrix m = new Matrix(2, 3, vals);
        // Debug.Log(m.ToString());

        // Debug.Log((m + m).ToString());
        // Debug.Log("-----------");

        float[] aVals = { 2, 0, 1, 2, 2, 1, 3, 5, 1, 1, 2, 2 };
        Matrix a = new Matrix(3, 4, aVals);
        float[] bVals = { 5, 2, 4, 1, 5, 5, 2, 2, 4, 5, 5, 1 };
        Matrix b = new Matrix(4, 3, bVals);
        Debug.Log(a.ToString());
        Debug.Log(b.ToString());
        Debug.Log((a * b).ToString());
        /* Answer: 22 16 14
                   42 40 30
                   20 21 19 */

        // float[] aVals = { 1, 2, 3, 4, 5, 6 };
        // Matrix a = new Matrix(2, 3, aVals);
        // float[] bVals = { 1, 2, 3, 4, 5, 6 };
        // Matrix b = new Matrix(3, 2, bVals);
        // Debug.Log(a.ToString());
        // Debug.Log(b.ToString());
        // Debug.Log((a * b).ToString());
        /* Answer: 22 28
                   49 64 */
    }

    // Update is called once per frame
    void Update()
    {

    }
}
