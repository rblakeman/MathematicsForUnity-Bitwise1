﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlanets : MonoBehaviour
{
    public int numOfPlanets = 2000;

    // Start is called before the first frame update
    void Start()
    {
        for (int idx = 0; idx < numOfPlanets; idx++) {
            GameObject planet = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            planet.transform.position = Random.insideUnitSphere * 1000;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
