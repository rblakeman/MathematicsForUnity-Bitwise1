﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    public float rotationSpeed = 1.0f;
    public float speed = 1.0f;

    /*
    *   Vertical up-down
    *   Horizontal left-right
    *   VerticalY = w-s (up/down) - fly forward/backward
    *   HorizontalZ = a-d (left/right)
    */

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        /* Translation */
        // float translateX = Input.GetAxis("Horizontal") * speed;
        // float translateY = Input.GetAxis("VerticalY") * speed;
        // float translateZ = Input.GetAxis("Vertical") * speed;

        // // this.transform.Translate(translateX, translateY, translateZ);

        // this.transform.position = new Vector3(
        //     this.transform.position.x + translateX,
        //     this.transform.position.y + translateY,
        //     this.transform.position.z + translateZ
        // );

        /* Rotation */
        // float rotateX = Input.GetAxis("Vertical") * rotationSpeed;
        // float rotateY = Input.GetAxis("Horizontal") * rotationSpeed;
        // float rotateZ = Input.GetAxis("HorizontalZ") * rotationSpeed;

        // this.transform.Rotate(rotateX, rotateY, rotateZ);


        /* Combined */
        float translateZ = Input.GetAxis("VerticalY") * speed;
        this.transform.Translate(0, 0, translateZ);
        float rotateX = Input.GetAxis("Vertical") * rotationSpeed;
        float rotateY = Input.GetAxis("Horizontal") * rotationSpeed;
        float rotateZ = Input.GetAxis("HorizontalZ") * rotationSpeed;

        this.transform.Rotate(rotateX, rotateY, rotateZ);
    }
}
