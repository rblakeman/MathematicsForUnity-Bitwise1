# Mathematics For Unity - Udemy

> Unity Version 2019.3.8f1

## Mathematics for Computer Games Development using Unity
### Udemy Course by: Penny de Byl
#### https://www.udemy.com/share/10157Y2@PW5gfUtfS1EIcERGBEtnfRRuYA==/

Course Content:
- Section 1: Introduction and Welcome
- Section 2: Bitwise Operations
- Section 3: Location
- Section 4: Vectors
- Section 5: Intersections
- Section 6: Affine Transformations
- Section 7: Final Words

Projects:
- Bitwise Operations (Section 2)
- Cartesian Coordinate Plane (Section 2/3)
- Vectors Tank 2D (Section 3/4)
- Aircraft 3D (Section 4)
- ParametricForm (Section 5)
- UnityWithoutCoordsExamples (Section 5)
- Matrices (Section 6)
- Quaternions (Section 6)

Ryan Blakeman ©2021

rblakeman31@gmail.com

https://ryanblakeman.com
