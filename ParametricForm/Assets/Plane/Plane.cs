﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane
{
    // P(s,t) = A + vs + ut
    public Coords A;
    Coords B;
    Coords C;
    public Coords v;
    public Coords u;

    // Take in 3 pts
    public Plane(Coords _A, Coords _B, Coords _C)
    {
        A = _A;
        B = _B;
        C = _C;
        v = B - A;
        u = C - A;
    }

    // Take in 1 pt and 2 vectors
    public Plane(Coords _A, Vector3 _V, Vector3 _U)
    {
        A = _A;
        v = new Coords(_V);
        u = new Coords(_U);
    }

    public Coords Lerp(float s, float t)
    {
        float xst = A.x + (v.x * s) + (u.x * t);
        float yst = A.y + (v.y * s) + (u.y * t);
        float zst = A.z + (v.z * s) + (u.z * t);

        return new Coords(xst, yst, zst);
    }
}
