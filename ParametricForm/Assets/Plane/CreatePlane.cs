﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlane : MonoBehaviour
{
    public Transform A;
    public Transform B;
    public Transform C;
    public float density = 0.1f;
    public float startBoundary = 0f;
    public float endBoundary = 1f;
    Plane plane;

    // Start is called before the first frame update
    void Start()
    {
        plane = new Plane(
            new Coords(A.position),
            new Coords(B.position),
            new Coords(C.position)
        );

        for (float s = startBoundary; s < endBoundary; s += density)
        {
            for (float t = startBoundary; t < endBoundary; t += density)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = plane.Lerp(s,t).ToVector();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
