using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWallReflection : MonoBehaviour
{
    Line ballPathLine;
    Line walllLine;
    float intersectT;
    float intersectS;
    bool collides = false;

    public GameObject ball;
    Line ballTrajectory;

    // Start is called before the first frame update
    void Start()
    {
        walllLine = new Line(new Coords(9, -12, 0), new Coords(0, 25, 0));
        walllLine.Draw(1, Color.blue);

        ballPathLine = new Line(new Coords(-11, 10, 0), new Coords(50, -20, 0));
        ballPathLine.Draw(0.1f, Color.yellow);

        ball.transform.position = ballPathLine.A.ToVector();

        intersectT = ballPathLine.IntersectsAt(walllLine);
        intersectS = walllLine.IntersectsAt(ballPathLine);
        Debug.Log(intersectT + " " + intersectS);
        if (!float.IsNaN(intersectT) && !float.IsNaN(intersectS)) {
            ballTrajectory = new Line(ballPathLine.A, ballPathLine.Lerp(intersectT), Line.LINETYPE.SEGMENT);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time <= 1) {
            ball.transform.position = ballTrajectory.Lerp(Time.time).ToVector();
        } else {
            ball.transform.position += ballTrajectory.Reflect(Coords.Perp(walllLine.v)).ToVector() * Time.deltaTime * 10;
        }

    }
}
