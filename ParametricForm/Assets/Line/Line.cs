﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line
{
    // L(t) = A + vt
    public Coords A;
    public Coords B;
    public Coords v;

    public enum LINETYPE { LINE, SEGMENT, RAY };
    LINETYPE type;

    // 2 Points
    public Line(Coords _A, Coords _B, LINETYPE _type)
    {
        A = _A;
        B = _B;
        type = _type;
        v = B - A;
    }

    // Point and a "Vector"
    public Line(Coords _A, Coords _V)
    {
        A = _A;
        v = _V;
        B = _A + _V;
        type = LINETYPE.SEGMENT;
    }

    // A + vt = B + us
    public float IntersectsAt(Line l)
    {
        // uPerp . v == 0
        if (HolisticMath.Dot(Coords.Perp2D(this.v), l.v) == 0) {
            Debug.Log("IntersectsAt failed, parallel lines");
            return float.NaN;
        }

        // C = B - A
        Coords tempB = l.A;
        Coords C = tempB - this.A;

        // t = (u-perp . C) / (u-perp . v)
        Coords uPerp = Coords.Perp2D(l.v);
        float uPerpDotC = HolisticMath.Dot(uPerp, C);
        float uPerpDotV = HolisticMath.Dot(uPerp, this.v);
        float t = uPerpDotC / uPerpDotV;

        if ((t < 0 || t > 1) && this.type == LINETYPE.SEGMENT) {
            Debug.Log("IntersectsAt failed, non-touching");
            return float.NaN;
        }

        return t;
    }

    // A + wt = B + va + ue
    // A + wt = H
    // n . (H - B) = 0
    // n . ((A + wt) - B) = 0
    // t = (-n.(A-B)) / (n.w)
    public float IntersectsAt(Plane p)
    {
        Coords w = this.v;
        Coords v = p.v;
        Coords u = p.u;
        Coords _B = p.A;

        Coords n = HolisticMath.CrossProduct(v, u);

        if (HolisticMath.Dot(n, w) == 0) {
            return float.NaN;
        }

        float t = (HolisticMath.Dot(-n, this.A - _B)) / (HolisticMath.Dot(n, w));

        if ((t < 0 || t > 1) && this.type == LINETYPE.SEGMENT) {
            Debug.Log("IntersectsAt failed, non-touching");
            return float.NaN;
        }

        return t;
    }

    // r = a - 2(a . n)n
    // a and n are normalized
    public Coords Reflect(Coords n) {
        Coords _aNorm = this.v.GetNormal();
        Coords nNorm = n.GetNormal();

        // if n.c == 0 then parallel
        // if n.c <= 0 then opposite side
        // if n.c >= 0 then facing
        if (HolisticMath.Dot(nNorm, _aNorm) <= 0) {
            return this.v;
        }

        // Coords r = _aNorm + (2f * HolisticMath.Dot(-_aNorm, nNorm) * nNorm);
        Coords r = _aNorm - (2f * HolisticMath.Dot(_aNorm, nNorm) * nNorm);

        return r;
    }

    public void Draw(float width, Color col)
    {
        Coords.DrawLine(A, B, width, col);
    }

    public Coords Lerp(float t)
    {
        switch (type) {
            case LINETYPE.LINE:
            // do nothing
                break;
            case LINETYPE.SEGMENT:
                if (t < 0) {
                    t = 0f;
                } else if (t > 1) {
                    t = 1f;
                }
                break;
            case LINETYPE.RAY:
                if (t < 0) {
                    t = 0f;
                } 
                break;
        }

        float xt = A.x + v.x * t;
        float yt = A.y + v.y * t;
        float zt = A.z + v.z * t;

        return new Coords(xt, yt, zt);
    }
}
