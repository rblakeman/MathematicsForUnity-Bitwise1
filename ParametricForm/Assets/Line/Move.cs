﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public float speed = 0.1f;
    // Line line;

    // Start is called before the first frame update
    void Start()
    {
        // line = new Line(
        //     new Coords(start.position.x, start.position.y, start.position.z),
        //     new Coords(end.position.x, end.position.y, end.position.z),
        //     Line.LINETYPE.SEGMENT
        // );
    }

    // Update is called once per frame
    void Update()
    {
        // this.transform.position = line.Lerp(Time.time * 0.1f).ToVector();
        this.transform.position = HolisticMath.LerpSegment(
            new Coords(start.position), new Coords(end.position), speed
        ).ToVector();
    }
}
