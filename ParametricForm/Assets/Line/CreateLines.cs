﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLines : MonoBehaviour
{
    // Touching Line Segments
    Vector3 start1 = new Vector3(-100, 0, 0);
    Vector3 end1 = new Vector3(200, 150, 0);
    Vector3 start2 = new Vector3(0, -100, 0);
    Vector3 end2 = new Vector3(0, 200, 0);
    Line L1;
    Line L2;

    // Parallel Lines (and Line Segments)
    Vector3 start3 = new Vector3(-100, 75, 0);
    Vector3 end3 = new Vector3(200, 0, 0);
    Vector3 start4 = new Vector3(-100, 100, 0);
    Vector3 end4 = new Vector3(200, 0, 0);
    Line L3;
    Line L4;

    // Non-Parallel Non-Touching Line Segments
    // Vector3 start5 = new Vector3(75, 100, 0);
    // Vector3 end5 = new Vector3(0, 100, 0);
    // Vector3 start6 = new Vector3(-50, 50, 0);
    // Vector3 end6 = new Vector3(200, 0, 0);
    Vector3 start5 = new Vector3(-50, 50, 0);
    Vector3 end5 = new Vector3(200, 0, 0);
    Vector3 start6 = new Vector3(75, 100, 0);
    Vector3 end6 = new Vector3(0, 100, 0);
    Line L5;
    Line L6;

    // Start is called before the first frame update
    void Start()
    {
        // Touching Line Segments
        L1 = new Line(new Coords(start1), new Coords(end1));
        L1.Draw(1, Color.green);
        L2 = new Line(new Coords(start2), new Coords(end2));
        L2.Draw(1, Color.green);

        float intersectT = L1.IntersectsAt(L2);
        float intersectS = L2.IntersectsAt(L1);
        Debug.Log(intersectT + " " + intersectS);
        if (!float.IsNaN(intersectT) && !float.IsNaN(intersectS)) {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = L1.Lerp(intersectT).ToVector();
        }

        // Parallel Lines (and Line Segments)
        L3 = new Line(new Coords(start3), new Coords(end3));
        L3.Draw(1, Color.red);
        L4 = new Line(new Coords(start4), new Coords(end4));
        L4.Draw(1, Color.red);

        float intersectT2 = L3.IntersectsAt(L4);
        float intersectS2 = L4.IntersectsAt(L3);
        Debug.Log(intersectT2 + " " + intersectS2);
        if (!float.IsNaN(intersectT2) && !float.IsNaN(intersectS2)) {
            GameObject sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere2.transform.position = L3.Lerp(intersectT2).ToVector();
        }

        // Non-Parallel Non-Touching Line Segments
        L5 = new Line(new Coords(start5), new Coords(end5));
        L5.Draw(1, Color.blue);
        L6 = new Line(new Coords(start6), new Coords(end6));
        L6.Draw(1, Color.blue);

        float intersectT3 = L5.IntersectsAt(L6);
        float intersectS3 = L6.IntersectsAt(L5);
        Debug.Log(intersectT3 + " " + intersectS3);
        if (!float.IsNaN(intersectT3) && !float.IsNaN(intersectS3)) {
            GameObject sphere3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere3.transform.position = L5.Lerp(intersectT3).ToVector();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
