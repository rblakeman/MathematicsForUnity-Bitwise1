﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolisticMath
{
    // Calculate a vector's distance/magnitude using origin
    static public float Distance(Coords pos) {
        float diffX = Square(pos.x);
        float diffY = Square(pos.y);
        float diffZ = Square(pos.z);

        float magnitude = Mathf.Sqrt(diffX + diffY + diffZ);

        return magnitude;
    }

    // Calculate a vector's distance/magnitude
    static public float Distance(Coords pos1, Coords pos2) {
        float diffX = Square(pos2.x - pos1.x);
        float diffY = Square(pos2.y - pos1.y);
        float diffZ = Square(pos2.z - pos1.z);

        float magnitude = Mathf.Sqrt(diffX + diffY + diffZ);

        return magnitude;
    }

    // Return the square of a given float
    static public float Square(float value) {
        return value * value;
    }

    // Calculate a vector's normal using origin
    static public Coords Normalize(Coords pos) {
        float magnitude = Distance(pos);
        float normalX = pos.x / magnitude;
        float normalY = pos.y / magnitude;
        float normalZ = pos.z / magnitude;

        return new Coords(normalX, normalY, normalZ);
    }

    // Calculate a vector's normal
    static public Coords calculateNormal(Coords pos1, Coords pos2) {
        float magnitude = Distance(pos1, pos2);
        float normalX = pos2.x / magnitude;
        float normalY = pos2.y / magnitude;
        float normalZ = pos2.z / magnitude;

        return new Coords(normalX, normalY, normalZ);
    }

    // Calculate the Dot Product of two vectors
    static public float Dot(Coords pos1, Coords pos2) {
        float dotX = pos1.x * pos2.x;
        float dotY = pos1.y * pos2.y;
        float dotZ = pos1.z * pos2.z;

        return dotX + dotY + dotZ;
    }

    // Calculate the Angle between two positions
    static public float Angle(Coords pos1, Coords pos2) {
        float dot = Dot(pos1, pos2);
        float magnitudes = Distance(pos1) * Distance(pos2);

        // Debug.Log("Angle() -- " + magnitudes + " = " + Distance(pos1) + " * " + Distance(pos2));

        if (magnitudes == 0) {
            Debug.LogWarning("Angle() -- Invalid Angle");
            return 0;
        }

        // Returns in Radians; Deg = Radians * (180/Mathf.Pi)
        return Mathf.Acos(dot / magnitudes);
    }

    // Calcuate the Rotation needed for a vector given an angle (in radians)
    static public Coords Rotate(Coords vector, float angle, bool clockwise = false) {
        if (clockwise) {
            angle = (2 * Mathf.PI) - angle;
        }

        float xVal = vector.x * Mathf.Cos(angle) - vector.y * Mathf.Sin(angle);
        float yVal = vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle);

        // Debug.Log("Rotate() -- " + xVal + ", " + yVal);

        return new Coords(xVal, yVal, 0f);
    }

    // Calculate a new position
    static public Coords Translate(Coords startPos, Coords dir, Coords newVec) {
        if (HolisticMath.Distance(new Coords(0, 0, 0), newVec) <= 0) {
            return startPos;
        }

        float angle = HolisticMath.Angle(newVec, dir);
        // If forwards returns 0, if reverse returns 180
        float worldAngle = HolisticMath.Angle(newVec, new Coords(0, 1, 0));

        bool clockwise = false;
        if (HolisticMath.CrossProduct(newVec, dir).z < 0) clockwise = true;

        Coords newVec2 = HolisticMath.Rotate(newVec, angle + worldAngle, clockwise);

        float xVal = startPos.x + newVec2.x;
        float yVal = startPos.y + newVec2.y;
        float zVal = startPos.z + newVec2.z;

        return new Coords(xVal, yVal, zVal);
    }

    static public Coords CrossProduct(Coords vec1, Coords vec2) {
        float xVal = (vec1.y * vec2.z) - (vec1.z * vec2.y);
        float yVal = (vec1.z * vec2.x) - (vec1.x * vec2.z);
        float zVal = (vec1.x * vec2.y) - (vec1.y * vec2.x);

        return new Coords(xVal, yVal, zVal);
    }

    static public Coords LookAt2D(Coords pos1, Coords pos2, Coords forwardVec) {
        Coords direction = new Coords(pos2.ToVector() - pos1.ToVector());

        // in radians
        float angle = Angle(forwardVec, direction);

        // Unity is 'left-handed' coordinate system, rotates left
        bool clockwise = false;
        if (CrossProduct(forwardVec, direction).z < 0) {
            clockwise = true;
        }

        return Rotate(forwardVec, angle, clockwise);
    }

    static public Coords LookAt2D(Coords direction, Coords forwardVec) {
        float angle = Angle(forwardVec, direction); // in radians

        // Unity is 'left-handed' coordinate system, rotates left
        bool clockwise = false;
        if (CrossProduct(forwardVec, direction).z < 0) {
            clockwise = true;
        }

        return Rotate(forwardVec, angle, clockwise);
    }

    // Convert a given angle from radians to degrees
    static public float ToDegrees(float angle) {
        return angle * (180 / Mathf.PI);
    }

    // Convert a given angle from degrees to radians
    static public float ToRadians(float angle) {
        return angle * (Mathf.PI / 180);
    }

    static public Coords Lerp(Coords start, Coords end, float speed)
    {
        float t = Time.time * speed;
        float xt = start.x + (end.x - start.x) * t;
        float yt = start.y + (end.y - start.y) * t;
        float zt = start.z + (end.z - start.z) * t;

        return new Coords(xt, yt, zt);
    }

    static public Coords LerpSegment(Coords start, Coords end, float speed)
    {
        float t = Time.time * speed;

        if (t < 0) {
            t = 0f;
        } else if (t > 1) {
            t = 1f;
        }

        float xt = start.x + (end.x - start.x) * t;
        float yt = start.y + (end.y - start.y) * t;
        float zt = start.z + (end.z - start.z) * t;

        return new Coords(xt, yt, zt);
    }

    static public Coords LerpRay(Coords start, Coords end, float speed)
    {
        float t = Time.time * speed;

        if (t < 0) {
            t = 0f;
        }

        float xt = start.x + (end.x - start.x) * t;
        float yt = start.y + (end.y - start.y) * t;
        float zt = start.z + (end.z - start.z) * t;

        return new Coords(xt, yt, zt);
    }
}
