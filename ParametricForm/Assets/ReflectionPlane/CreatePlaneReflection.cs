﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlaneReflection : MonoBehaviour
{
    public Transform B;
    public Transform D;
    public Transform E;
    Plane wall;
    Coords n;

    public Transform A;
    public Transform C;
    Line path;
    GameObject Hit;

    public GameObject ball;
    Line trajectory;

    // Start is called before the first frame update
    void Start()
    {
        wall = new Plane(
            new Coords(B.position),
            new Coords(D.position), 
            new Coords(E.position)
        );
        n = HolisticMath.CrossProduct(wall.v, wall.u);

        // draw plane grid
        for (float s = 0; s < 1; s += 0.1f) {
            for (float t = 0; t < 1; t += 0.1f) {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = wall.Lerp(s,t).ToVector();
            }
        }

        // draw path
        path = new Line(
            new Coords(A.position),
            new Coords(C.position),
            Line.LINETYPE.RAY
        );
        path.Draw(1, Color.green);
        ball.transform.position = path.A.ToVector();

        // calculate hit
        float intersectT = path.IntersectsAt(wall);
        if (!float.IsNaN(intersectT)) {
            Hit = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Hit.transform.position = path.Lerp(intersectT).ToVector();
            trajectory = new Line(path.A, new Coords(Hit.transform.position), Line.LINETYPE.SEGMENT);
            Debug.Log(Hit.transform.position);

            // Draw hit normal
            Line hitNormal = new Line(new Coords(Hit.transform.position), n, Line.LINETYPE.SEGMENT);
            hitNormal.Draw(0.5f, Color.blue);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time <= 1) {
            ball.transform.position = trajectory.Lerp(Time.time).ToVector();
        } else {
            ball.transform.position += trajectory.Reflect(n).ToVector() * Time.deltaTime * 10;
        }
    }
}
