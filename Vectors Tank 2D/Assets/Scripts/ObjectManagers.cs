﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManagers : MonoBehaviour
{
    public GameObject objPrefab;
    public Vector3 randomPos;

    // Start is called before the first frame update
    void Awake()
    {
        randomPos = new Vector3(
            Random.Range(-100, 100),
            Random.Range(-100, 100),
            objPrefab.transform.position.z
        );
        GameObject obj = Instantiate(objPrefab, randomPos, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
