﻿using UnityEngine;
using System.Collections;

// Originally 'Drive' from the math exercises
public class LookAt : MonoBehaviour
{
    public GameObject fuel;
    public float speed = 5f;
    public float stoppingDistance = 0.1f;
    Vector3 direction;

    Coords Up = new Coords(0,1,0);
    Coords Down = new Coords(0,-1,0);
    Coords Left = new Coords(-1,0,0);
    Coords Right = new Coords(1,0,0);

    void Start() {
        direction = fuel.transform.position - this.transform.position;

        Coords dirNormal = HolisticMath.Normalize(new Coords(direction));
        direction = dirNormal.ToVector();

        // this.transform.up = HolisticMath.LookAt2D(
        //     new Coords(this.transform.position),
        //     new Coords(fuel.transform.position),
        //     new Coords(this.transform.up)
        // ).ToVector();

        this.transform.up = HolisticMath.LookAt2D(
            dirNormal,
            new Coords(this.transform.up)
        ).ToVector();
    }

    void Update()
    {
        // Vector3 position = this.transform.position;

        // if (Input.GetKey(KeyCode.W)) {
        //     position.x += up.x;
        //     position.y += up.y;
        // }

        // if (Input.GetKey(KeyCode.S)) {
        //     position.x -= up.x;
        //     position.y -= up.y;
        // }

        // if (Input.GetKey(KeyCode.D)) {
        //     position.x += right.x;
        //     position.y += right.y;
        // }

        // if (Input.GetKey(KeyCode.A)) {
        //     position.x -= right.x;
        //     position.y -= right.y;
        // }

        // this.transform.position = position;

        // Vector3 direction = fuel.transform.position - this.transform.position;
        if (HolisticMath.Distance(new Coords(this.transform.position), new Coords(fuel.transform.position)) > stoppingDistance) {
            this.transform.position += direction * speed * Time.deltaTime;
        }
    }
}