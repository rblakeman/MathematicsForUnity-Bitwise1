﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Drive : MonoBehaviour
{
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public Text energyAmountLabel;
    Vector3 currentLocation;

    void ManualTranslate(float x, float y, float z) {
        float newX = this.transform.position.x + x;
        float newY = this.transform.position.y + y;
        float newZ = this.transform.position.z + z;

        this.transform.position = new Vector3(newX, newY, newZ);
        return;
    }

    void Start() {
        currentLocation = this.transform.position;
    }

    void Update()
    {
        // if (float.Parse(energyAmountLabel.text) <= 0) {
        //     energyAmountLabel.text = "0";

        //     return;
        // }

        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        // Move translation along the object's z-axis
        // this.transform.translate(0, translation, 0);
        // this.transform.position += transform.up * translation; //another options
        this.transform.position = HolisticMath.Translate(
            new Coords(this.transform.position),
            new Coords(this.transform.up),
            new Coords(0, translation, 0)
        ).ToVector();

        // Rotate around our y-axis
        // this.transform.rotate(0, 0, -rotation);
        this.transform.up = HolisticMath.Rotate(
            new Coords(this.transform.up),
            HolisticMath.ToRadians(-rotation),
            false
        ).ToVector();

        // Calculate distance
        float diffX = Mathf.Pow(currentLocation.x - this.transform.position.x, 2);
        float diffY = Mathf.Pow(currentLocation.y - this.transform.position.y, 2);
        float distance = Mathf.Sqrt(diffX + diffY);

        energyAmountLabel.text = (float.Parse(energyAmountLabel.text) - distance + "");

        currentLocation = this.transform.position;
    }
}