﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject tank;
    public GameObject fuel;
    public Text tankPosText;
    public Text fuelPosText;
    public Text energyAmountLabel;
    public Text turnAngleLabel;

    public void AddEnergy(string amount) {
        float n;
        if (float.TryParse(amount, out n)) {
            float newEnergy = float.Parse(amount);
            energyAmountLabel.text = (float.Parse(energyAmountLabel.text) + newEnergy + "");

            if(float.Parse(energyAmountLabel.text) > 100) {
                energyAmountLabel.text = "100";
            }
        }
    }

    // Add Rotatation
    public void TurnAngle(string degrees) {
        float n;
        if (float.TryParse(degrees, out n)) {
            float newRads = HolisticMath.ToRadians(float.Parse(degrees));

            tank.transform.up = HolisticMath.Rotate(new Coords(tank.transform.up), newRads, true).ToVector();
        }
    }

    // Set Rotation from origin
    public void SetAngle(string degrees) {
        float n;
        if (float.TryParse(degrees, out n)) {
            Vector3 currentRotation = tank.transform.rotation.eulerAngles;
            float newRotation = float.Parse(degrees);

            if (newRotation != currentRotation.z) {
                currentRotation.z = newRotation;
                tank.transform.rotation = Quaternion.Euler(currentRotation);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Coords fuelPos = new Coords(fuel.GetComponent<ObjectManagers>().randomPos);
        Coords tankPos = new Coords(tank.transform.position);
        Coords tankDir = new Coords(tank.transform.up);

        tankPosText.text = "(" + tank.transform.position.x + "," + tank.transform.position.y + ")";
        fuelPosText.text = "(" + fuelPos.x + "," + fuelPos.y + ")";

        float angle = HolisticMath.ToDegrees(HolisticMath.Angle(tankDir, fuelPos));

        // angle always returns 0<=x<=180, check if it is to the left or right of tank
        if (HolisticMath.Dot(new Coords(tank.transform.right), fuelPos) < 0) {
            angle = 360 - angle;
        }

        Debug.Log("Energy Needed: " + HolisticMath.Distance(tankPos, fuelPos));
        Debug.Log("Angle Needed: " + angle);
    }

    void Update()
    {
        
    }
}
