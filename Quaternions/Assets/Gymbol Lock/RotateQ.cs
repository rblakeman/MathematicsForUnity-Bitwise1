﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateQ : MonoBehaviour
{
    void Start() {
        /*
        // q = cos(theta/2) + aNorm * sin(theta/2)
        float angleDeg = 45;
        float angleRadians = angleDeg * Mathf.Deg2Rad;
        float cos = Mathf.Cos(angleRadians / 2.0f);
        float sin = Mathf.Sin(angleRadians / 2.0f);
        Vector3 A = new Vector3(2, 1, 5);
        Vector3 aNormalized = Vector3.Normalize(A);
        Debug.Log("normalized " + aNormalized);
        Vector3 aNormSin = new Vector3(aNormalized.x * sin, aNormalized.y * sin, aNormalized.z * sin);
        Debug.Log("q: " + aNormSin);
        Debug.Log("w: " + cos);
        */

        Coords asdf = new Coords(1, 1, 1);
        HolisticMath.QRotate(new Coords(0,0,0), asdf, 1);

        // Quaternion q = Quaternion.AngleAxis(45, A);
        // Debug.Log("checksum: " + q.x + " " + q.y + " " + q.z);
        // Debug.Log("checksum: " + q.w);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(1, 1, 1);
    }
}
