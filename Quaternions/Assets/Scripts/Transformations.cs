﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Transformations : MonoBehaviour
{
    [Header("Single Point")]
    public GameObject point;
    public Vector3 pointTranslation;

    [Header("Object")]
    public GameObject centerPoint;
    public GameObject[] points;
    public Vector3 cubeTranslation;
    public Vector3 scaling;
    public Vector3 eulerAngles;
    public Vector3 shear;
    public bool reflectX;
    public bool reflectY;
    public bool reflectZ;

    // Start is called before the first frame update
    void Start()
    {
        DrawHouseLines(points, Color.white);

        /* Single Point */
        Coords pointPosition = new Coords(point.transform.position, 1);
        GameObject newSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        newSphere.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
        newSphere.transform.localScale = point.transform.localScale;

        newSphere.transform.position = HolisticMath.Translate(
            pointPosition,
            new Coords(pointTranslation, 0)
        ).ToVector();

        /* An object (multiple points) */
        Vector3 centerOffset = centerPoint.transform.position;
        Debug.Log(eulerAngles);
        Vector3 radians = eulerAngles * Mathf.Deg2Rad;
        Debug.Log(radians);

        int idx = 0;
        GameObject[] newPoints = new GameObject[points.Length];
        foreach (GameObject p in points) {
            Coords cubePosition = new Coords(p.transform.position, 1);
            GameObject newSpheres = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            newSpheres.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
            newSpheres.transform.localScale = p.transform.localScale;

            /*
            // TRANSLATE
            newSpheres.transform.position = HolisticMath.Translate(
                cubePosition,
                new Coords(cubeTranslation, 0)
            ).ToVector();
            */

            /*
            // SCALE
            // 1. move to origin
            cubePosition = HolisticMath.Translate(
                cubePosition,
                new Coords(-centerOffset, 0)
            );
            // 2. scale as desired
            cubePosition = HolisticMath.Scale(
                cubePosition,
                new Coords(scaling, 0)
            );
            // 3. move back to relative pos
            newSpheres.transform.position = HolisticMath.Translate(
                cubePosition,
                new Coords(centerOffset, 0)
            ).ToVector();
            */

            /*
            // ROTATE
            // 1. move to origin
            cubePosition = HolisticMath.Translate(
                cubePosition,
                new Coords(-centerOffset, 0)
            );
            // 2. Rotate as desired
            // cubePosition = HolisticMath.Rotate(
            //     cubePosition, radians.x, true,
            //                   radians.y, true,
            //                   radians.z, true);
            cubePosition = HolisticMath.Rotate(cubePosition, radians, true);
            // 3. move back to relative pos
            newSpheres.transform.position = HolisticMath.Translate(
                cubePosition,
                new Coords(centerOffset, 0)
            ).ToVector();
            */

            /*
            // SHEAR
            newSpheres.transform.position = HolisticMath.Shear(
                cubePosition,
                shear
            ).ToVector();
            */

            // REFLECTION
            Vector3 reflection = new Vector3(1, 1, 1);
            if (reflectX) {
                reflection.x = -1f;
            }
            if (reflectY) {
                reflection.y = -1f;
            }
            if (reflectZ) {
                reflection.z = -1f;
            }
            newSpheres.transform.position = HolisticMath.Reflection(
                cubePosition,
                reflection
            ).ToVector();

            newPoints[idx] = newSpheres;
            idx++;
        }
        DrawHouseLines(newPoints, Color.cyan);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void DrawHouseLines(GameObject[] fakePoints, Color color) {
        // Draw the white lines
        Coords.DrawLine(new Coords(fakePoints[0].transform.position), new Coords(fakePoints[1].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[1].transform.position), new Coords(fakePoints[2].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[2].transform.position), new Coords(fakePoints[3].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[3].transform.position), new Coords(fakePoints[0].transform.position), 0.02f, color);

        Coords.DrawLine(new Coords(fakePoints[4].transform.position), new Coords(fakePoints[5].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[5].transform.position), new Coords(fakePoints[6].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[6].transform.position), new Coords(fakePoints[7].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[7].transform.position), new Coords(fakePoints[4].transform.position), 0.02f, color);

        Coords.DrawLine(new Coords(fakePoints[1].transform.position), new Coords(fakePoints[5].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[6].transform.position), new Coords(fakePoints[2].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[0].transform.position), new Coords(fakePoints[4].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[7].transform.position), new Coords(fakePoints[3].transform.position), 0.02f, color);

        Coords.DrawLine(new Coords(fakePoints[8].transform.position), new Coords(fakePoints[1].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[8].transform.position), new Coords(fakePoints[2].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[8].transform.position), new Coords(fakePoints[5].transform.position), 0.02f, color);
        Coords.DrawLine(new Coords(fakePoints[8].transform.position), new Coords(fakePoints[6].transform.position), 0.02f, color);
    }
}
