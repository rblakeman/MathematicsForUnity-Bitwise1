﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisRotate : MonoBehaviour
{
    public GameObject[] points;
    public Vector3 angle;
    public bool clockwiseRotation;

    // Start is called before the first frame update
    void Start()
    {
        angle = angle * Mathf.Deg2Rad;

        foreach(GameObject p in points) {
            Coords position = new Coords(p.transform.position, 1);
            p.transform.position = HolisticMath.Rotate(position,
                                                       angle.x, clockwiseRotation,
                                                       angle.y, clockwiseRotation,
                                                       angle.z, clockwiseRotation).ToVector();
        }

        Matrix rotationMatrix = HolisticMath.GetRotationMatrix(
            angle.x, clockwiseRotation,
            angle.y, clockwiseRotation,
            angle.z, clockwiseRotation
        );
        float rotationAngle = HolisticMath.GetRotationAxisAngle(rotationMatrix);
        Debug.Log(rotationAngle * Mathf.Rad2Deg);
        Coords rotationAxis = HolisticMath.GetRotationAxis(rotationMatrix, rotationAngle);
        Debug.Log(rotationAxis.ToString());
        Coords.DrawLine(new Coords(0,0,0), rotationAxis * 5, 0.1f, Color.grey);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
