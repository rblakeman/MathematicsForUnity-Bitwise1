﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawGraph : MonoBehaviour
{
    public int graphX = 15;
    public int graphY = 6;
    public int offset = 10;

    void Start()
    {
        Coords xStartPoint = new Coords(-160,0, gameObject.transform);
        Coords xEndPoint = new Coords(160, 0, gameObject.transform);
        Coords yStartPoint = new Coords(0, 100, gameObject.transform);
        Coords yEndPoint = new Coords(0, -100, gameObject.transform);

        Coords.DrawLine(xStartPoint, xEndPoint, 1, Color.red);
        Coords.DrawLine(yStartPoint, yEndPoint, 1, Color.green);

        for (int x = -graphX; x <= graphX; x++)
        {
            Coords.DrawLine(new Coords(x*offset, graphY*offset, 0, gameObject.transform), new Coords(x*offset, graphY*-offset, 0, gameObject.transform), 0.5f, Color.white);
        }
        for (int y = -graphY; y <= graphY; y++)
        {
            Coords.DrawLine(new Coords(graphX*-offset, y*offset, 0, gameObject.transform), new Coords(graphX*offset, y*offset, 0, gameObject.transform), 0.5f, Color.white);
        }
    }

    void Update()
    {
        
    }
}
