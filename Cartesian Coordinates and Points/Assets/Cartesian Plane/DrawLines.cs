﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLines : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        List<Coords> aquarius = new List<Coords>()
        {
            new Coords(100,5, gameObject.transform),
            new Coords(80,15, gameObject.transform),
            new Coords(70,25, gameObject.transform),
            new Coords(45,20, gameObject.transform),
            new Coords(40,5, gameObject.transform),
            new Coords(10,45, gameObject.transform),
            new Coords(20,60, gameObject.transform),
            new Coords(30,65, gameObject.transform),
            new Coords(30,85, gameObject.transform),
            new Coords(60,65, gameObject.transform),
            new Coords(90,70, gameObject.transform),
            new Coords(60,65, gameObject.transform),
            new Coords(30,85, gameObject.transform),
            new Coords(60,105, gameObject.transform),
            new Coords(100,140, gameObject.transform)
        };

        // Debug.Log(point.ToString());
        // Coords.DrawPoint(new Coords(0,0), 10, Color.yellow);
        // Coords.DrawPoint(new Coords(10, 20), 2, Color.blue);

        // Draw Aquarius star sign
        for (int i = 0; i < aquarius.Count-1; i++)
        {
            Coords.DrawLine(aquarius[i], aquarius[i+1], 1, Color.blue);
        }
    }

    void Update()
    {
        
    }
}
