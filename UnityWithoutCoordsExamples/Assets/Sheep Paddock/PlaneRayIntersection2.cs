﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneRayIntersection2 : MonoBehaviour
{
    public GameObject sheep;

    public GameObject quad;
    public GameObject[] fences;
    Vector3[] fenceNormals;
    Plane mPlane;
    // float BOUNDS;

    // Start is called before the first frame update
    void Start()
    {
        Vector3[] verts = quad.GetComponent<MeshFilter>().mesh.vertices;
        mPlane = new Plane(
            quad.transform.TransformPoint(verts[0]) + new Vector3(0, 0.2f, 0),
            quad.transform.TransformPoint(verts[1]) + new Vector3(0, 0.2f, 0),
            quad.transform.TransformPoint(verts[2]) + new Vector3(0, 0.2f, 0)
        );

        fenceNormals = new Vector3[fences.Length];
        for(int idx = 0; idx < fences.Length; idx++) {
            Vector3 normal = fences[idx].GetComponent<MeshFilter>().mesh.normals[0];
            fenceNormals[idx] = fences[idx].transform.TransformVector(normal);
        }

        // // Boundary offset
        // BOUNDS = Mathf.Abs(quad.transform.TransformPoint(verts[0]).x);
        // if (BOUNDS == 0f) {
        //     Mathf.Abs(quad.transform.TransformPoint(verts[0]).z);
        // }
        // BOUNDS = BOUNDS -= 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float t = 0.0f;

            if (mPlane.Raycast(ray, out t)) {
                Vector3 hitPoint = ray.GetPoint(t);

                // Prevent clicking outside of boundary
                // if (hitPoint.x > -BOUNDS && hitPoint.x < BOUNDS &&
                //     hitPoint.z > -BOUNDS && hitPoint.z < BOUNDS)
                // {
                //     sheep.transform.position = hitPoint;
                // }
                bool inside = true;
                for(int i = 0; i < fences.Length; i++) {
                    Vector3 hitPointToFence = fences[i].transform.position - hitPoint;
                    Debug.DrawLine(hitPoint, fences[i].transform.position, Color.red);
                    Debug.DrawLine(fences[i].transform.position, fences[i].transform.position + fenceNormals[i], Color.blue);
                    Debug.Log(i);
                    Debug.Log(fences[i].transform.position);
                    Debug.Log(fenceNormals[i]);
                    inside = inside && Vector3.Dot(hitPointToFence, fenceNormals[i]) <= 0;
                }

                if (inside) {
                    sheep.transform.position = hitPoint;
                }
            }
        }
    }
}
